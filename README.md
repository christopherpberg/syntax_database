syntax_database
=========

Provisions:		
- AWS RDS database service


Requirements
------------
N/A

Role Variables
--------------
N/A

Dependencies
------------
AWS CLI

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: awscli
      roles:
     - { role: syntax_database, rds.user=${DB_USER}, rds.password=${DB.PASSWORD} }

License
-------

GNU

See also
------------------
N/A
